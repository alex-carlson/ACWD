'use strict';
module.exports = function(grunt) {

    grunt.initConfig({

        // watch for changes and trigger compass, jshint, uglify and livereload
        watch: {
            options: {
              livereload: false,
            },
            compass: {
                files: ['_assets/styles/**/*.{scss,sass}'],
                tasks: ['compass']
            },
            js: {
                files: '<%= jshint.all %>',
                tasks: ['jshint', 'uglify']
            },
            images: {
                files: ['_assets/images/**/*.{png,jpg,gif}'],
                tasks: ['imagemin']
            },
			fonts:{
                files: ['_assets/fonts/'],
                tasks: ['copy']
			}
        },

        // compass and scss
        compass: {
            dist: {
                options: {
                    config: 'config.rb',
                    force: true
                }
            }
        },
		
		//Copy font files to _public directory
		copy: {
		  main: {
		    files: [
		      {expand: true, flatten: true, src: ['_assets/fonts/**'], dest: '_public/', filter: 'isFile'}
		    ]
		  }
		},

        // javascript linting with jshint
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                "force": true
            },
            all: [
                'Gruntfile.js',
                '_assets/js/**/*.js'
            ]
        },
		
		//Slim
		slim: {
			dist: {
				files: {
					'_static/index.html': '_assets/slim/index.slim'
				}
			}
		},

        // uglify to concat, minify, and make source maps
        uglify: {
            plugins: {
                files: {
                    '_public/plugins.js': [
                        '_assets/js/vendor/jquery-1.10.1.min.js', 
                        '_assets/js/vendor/jquery.hotkeys.js', 
                        '_assets/js/vendor/jquery-migrate-1.2.1.min.js',
                        '_assets/js/vendor/jquery.fancybox.js',
                        '_assets/js/vendor/skrollr.min.js',
                        '_assets/js/vendor/konami.js',
                        //'_assets/js/vendor/masonry.pkgd.min.js',
                        '_assets/js/vendor/jquery.easing-1.3.js', 
                        '_assets/js/vendor/jquery.royalslider.min.js',
                        //'_assets/js/vendor/jquery.parallax.js',
                        '_assets/js/vendor/typed.js',
                    ]
                }
            },
            main: {
                files: {
                    '_public/main.js': [
                        '_assets/js/main.js',
                    ]
                }
            }
        },

        // image optimization
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7,
                    progressive: true,
                    interlaced: true
                },
                files: [{
                    expand: true,
                    cwd: '_assets/images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: '_public/'
                }]
            }
        },

    });
	
    // load all grunt tasks matching the `grunt-*` pattern
    require('load-grunt-tasks')(grunt);

    // register task
    grunt.registerTask('default', ['compass', 'uglify', 'imagemin', 'watch', 'copy']);

};