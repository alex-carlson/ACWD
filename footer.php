		</div>
	</div>
		
	<footer class="main col full cf" id="Contact">
		<div class="innerWrapper cf">
			<div class="col half">
				<h2>Contact</h2>
				<a href="mailto:alex@acwd.me" target="_blank">alex@acwd.me</a><br>
				<a href="tel:9528187931">952.818.7931</a>
			</div>
			<div class="col half right">
				<h2>Connect</h2>
				<a href="//www.facebook.com/acarlsongraphics" target="_blank"><i class="fa fa-facebook"></i></a>
				<a href="//twitter.com/alexcgraphics" target="_blank"><i class="fa fa-twitter"></i></a>
				<a href="//gitlab.com/u/alexcgraphics" target="_blank"><i class="fa fa-git"></i></a>
				<a href="//linkedin.com/in/carlsonalex" target="_blank"><i class="fa fa-linkedin"></i></a>
			</div>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>

<!-- <script type="text/javascript">
    if ($(window).width() > 1024) {
        var s = skrollr.init({forceHeight: false});
    }
</script> -->

</body>

<script src='http://jeromeetienne.github.io/threex.terrain/examples/vendor/three.js/build/three-min.js'></script>
<script src='http://jeromeetienne.github.io/threex.terrain/examples/vendor/three.js/examples/js/SimplexNoise.js'></script>
<script src='http://jeromeetienne.github.io/threex.terrain/threex.terrain.js'></script>

<script src="<?php echo get_template_directory_uri(); ?>/_public/main.js"></script>
<script>

	wireframeColor = secretColor;

	var renderer	= new THREE.WebGLRenderer({
		antialias	: true
	});
	/* Let's get this to render on the entire screen dimension */
	renderer.setSize( window.innerWidth, window.innerHeight );
	/* Add it to HTML */
	document.getElementById('canvas').appendChild( renderer.domElement );
	var onRenderFcts= [];
	var scene	= new THREE.Scene();
	var camera	= new THREE.PerspectiveCamera(25, window.innerWidth /    window.innerHeight, 0.01, 1000);
	var mouse = new THREE.Vector2(), INTERSECTED;
	var radius = 100, theta = 0;
	/* Played around a lot with camera positions, liked this setup the most */
	camera.position.z = 15;
	camera.position.y = 2;

	/* Let's get some fog in here for the part in the back */
	scene.fog = new THREE.Fog(0x000, 0, 45);
	;(function(){
		// add a ambient light
		var light	= new THREE.AmbientLight( 0x202020 )
		scene.add( light )
		// add a light in front
		var light	= new THREE.DirectionalLight('white', 5)
		light.position.set(0.5, 0.0, 2)
		scene.add( light )
		// add a light behind
		var light	= new THREE.DirectionalLight('white', 0.75*2)
		light.position.set(-0.5, -0.5, -2)
		scene.add( light )
	})()

	window.addEventListener( 'resize', onWindowResize, false );

	function onWindowResize() {

		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();

		renderer.setSize( window.innerWidth, window.innerHeight );

	}

	var heightMap	= THREEx.Terrain.allocateHeightMap(256,256)
	THREEx.Terrain.simplexHeightMap(heightMap)
	var geometry	= THREEx.Terrain.heightMapToPlaneGeometry(heightMap)
	THREEx.Terrain.heightMapToVertexColor(heightMap, geometry)
	/* Wireframe built-in color is white, no need to change that */
	var material	= new THREE.MeshBasicMaterial({
		wireframe: true,
		color: wireframeColor
	});
	var mesh	= new THREE.Mesh( geometry, material );
	scene.add( mesh );
	mesh.lookAt(new THREE.Vector3(0,1,0));
	/* Let's get some nice terrain */
	mesh.scale.y	= 3.5;
	mesh.scale.x	= 3;
	mesh.scale.z	= 0.20;
	mesh.scale.multiplyScalar(10);

	/* Slowly spin it around so that it doesn't look bland */
	onRenderFcts.push(function(delta, now){
		mesh.rotation.z += 0.1 * delta;
	})
	onRenderFcts.push(function(){

		renderer.render( scene, camera );
	})

	var lastTimeMsec= null
	requestAnimationFrame(function animate(nowMsec){
		requestAnimationFrame( animate );
		lastTimeMsec	= lastTimeMsec || nowMsec-1000/60
		var deltaMsec	= Math.min(200, nowMsec - lastTimeMsec)
		lastTimeMsec	= nowMsec
		onRenderFcts.forEach(function(onRenderFct){
			onRenderFct(deltaMsec/1000, nowMsec/1000)
		})
	})
	
	function render() {

				theta += 0.1;

				camera.position.x = radius * Math.sin( THREE.Math.degToRad( theta ) );
				camera.position.y = radius * Math.sin( THREE.Math.degToRad( theta ) );
				camera.position.z = radius * Math.cos( THREE.Math.degToRad( theta ) );

				camera.lookAt( scene.position );

				camera.updateMatrixWorld();

				renderer.render( scene, camera );

				console.log(secretColor);

			}
</script>

</html>