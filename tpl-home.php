<?php
/*Template Name: Home Template*/
?>

<?php get_header(); ?>
<div class="title">
  <h1>Alex Carlson</h1>
  <h2>Web / Game Developer</h2>
</div>
<div class="scrollDown">
  <a href="#About"><i class="fa fa-angle-down"></i></a>
</div>
<section class="col full" id="canvas">
</section>
<section class="col full" style="position: relative; box-shadow: 0 10px 30px 2px #000;">
	<div class="innerWrapper">
		<span id="About"></span>
	  <div class="col angled">
	    <div class="profile">
	      <div class="col half" style="text-align: center">
					<img src="https://graph.facebook.com/acarlsongraphics/picture?type=large">
	      </div>
        <div class="col half">
          <p>
            Alex Carlson is a development focused <span class="red">creative</span> who builds 
            <span class="red">accessible</span> applications and games that provide new
            forms of interaction to the user.  He is passionate about user experience and good <span class="red">development</span> practices.
          </p>
          <p>
            <a class="fancybox fancybox.fancypdf fancypdf" rel="resume" href="http://acwd.me/wp-content/uploads/2015/02/Resume1.pdf">Resume</a>
            <a class="fancybox fancybox.fancypdf fancypdf" rel="bio" href="http://acwd.me/wp-content/uploads/2015/02/ArtistBio-1.pdf">Artist Statement</a>
          </p>
        </div>
        <!-- <hr> -->
<!--         <div class="col half">
          <div class="console">
            <span>Alex Carlson$ </span><span id="typer">Developer</span>
          </div>
        </div>
        <div class="col half">
        </div> -->
	    </div>
	  </div>
	</div>
</section>

<section class="col full gray fixedImage">
  <span id="Skills"></span>
    <div class="innerWrapper">
      <h1 class="red">My Toolbelt</h1>
      <div class="table">
        <div class="col double">
          <h4>Languages</h4>
          <ul>
            <li>HTML</li>
            <li>CSS</li>
            <li>Javascript</li>
            <li>JQuery</li>
            <li>Ruby</li>
            <li>C#</li>
            <li>d3.js</li>
            <li>Three.js</li>
          </ul>
        </div>
        <div class="col">
          <h4>Platforms</h4>
          <ul>
            <li>Arduino</li>
            <li>Rapberry Pi</li>
            <li>Android</li>
          </ul>
        </div>
        <div class="col double">
          <h4>Applications</h4>
          <ul>
            <li>Unity</li>
            <li>Blender</li>
            <li>Processing</li>
            <li>TextMate</li>
            <li>Sublime Text</li>
            <li>Photoshop</li>
            <li>Illustrator</li>
          </ul>
        </div>
      </div>
    </div>
</section>
<section class="col full work">
  <span id="Projects"></span>
  <div class="innerWrapper">
    <div class="royalSlider">
      <?php
        $temp = $wp_query; $wp_query= null;
        $wp_query = new WP_Query(); $wp_query->query('showposts=30' . '&paged='.$paged);
        $i = 0;
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

        <div class="item rsContent <?php echo $i ?>">

        <?php
$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
$url = $thumb['0'];
?>

          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          <?php the_content(); ?>
          <div class="rsTmb">
            <span class="hidden"><?php the_title(); ?></span>
            <?php echo get_the_post_thumbnail( $page->ID, 'thumbnail' ); ?>
          </div>
        </div>

      <?php $i++; endwhile; ?>

      <?php wp_reset_postdata(); ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>	