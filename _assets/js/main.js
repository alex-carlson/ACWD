secretColor = "#ffffff";

$('header.mobile i.fa-bars').click(function(){
	$('.mobile nav').toggleClass('hidden');
});

$(function(){
    $("#typer").typed({
      strings: ["Web Developer", "Game Developer", "Artist",
      "Pizzatarrian", "Adventurer", "Guitarist"],
      loop: true
    });
});

if( $( window ).width() > 1080 ){

  //royalslider init
  $('.royalSlider').royalSlider({
    autoHeight: true,
    autoScaleSlider:false,
    imageScaleMode:'fit',
    transitionType: 'fade',
    controlNavigation: 'thumbnails',
    navigateByClick: false,
    sliderDrag: false,
    thumbs: {
        // thumbnails options go gere
        spacing: 10,
        orientation: 'vertical'
      }
  });

} else if( $( window ).width() > 800 ) {
  //royalslider init
  $('.royalSlider').royalSlider({
    autoHeight: true,
    autoScaleSlider:false,
    imageScaleMode:'fit',
    transitionType: 'move',
    controlNavigation: 'thumbnails',
    navigateByClick: false,
    sliderDrag: true,
    thumbs: {
        // thumbnails options go gere
        spacing: 4,
        orientation: 'horizontal'
      }
  });
} else {
  //royalslider init
  $('.royalSlider').royalSlider({
    autoHeight: true,
    autoScaleSlider:false,
    imageScaleMode:'fit',
    transitionType: 'move',
    controlNavigation: 'thumbnails',
    navigateByClick: false,
    sliderDrag: true,
    thumbs: {
        // thumbnails options go gere
        spacing: 1,
        orientation: 'horizontal'
      }
  });
}

// $(document).scroll(function() {
//   if($(document).scrollTop() > $(window).height()){
//     $('header').addClass('hidden');
//   } else {
//     $('header').removeClass('hidden');
//   }
// });

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

$('svg').on('click', function(){
  var obj = document.createElement("audio"); 
  var sounds = Math.floor(Math.random()*(3-1+1)+1);
  obj.setAttribute("src", templateUrl+"/_public/audio"+sounds+".wav"); 
  $.get(); 
  obj.play(); 
});

$(".fancypdf").click(function(){
 $.fancybox({
    type: 'html',
    autoSize: false,
    closeClick  : true,
   content: '<embed src="'+this.href+'#nameddest=self&page=1&view=FitH,0&zoom=100,0,0" type="application/pdf" height="100%" width="100%" />',
 }); //fancybox
 return false;
}); //click

$.konami(function() {
      var obj = document.createElement("audio"); 
      obj.setAttribute("src", templateUrl+"/_public/secret.wav"); 
      $.get(); 
      obj.play(); 
      //alert('Woooooo');
      $('*').css('color', '#29cb4a');
      $('svg').css('background', 'none');
      $('polygon').css('fill', '#000');
      $('polygon#red').css('fill', '#29cb4a');
      $('*').css('background', '#000');
      $('.rsSlide a:before').css('color', '#29cb4a');
      $('.rsSlide a').css('color', 'transparent');
      secretColor = '#29cb4a';
});