<?php get_header(); ?>		

			<?php // the loop ?>
			<?php if (have_posts()) : ?>

				<div class="innerWrapper">
					<section class="postLoop col">
						<?php while (have_posts()) : the_post(); ?>

							<?php get_template_part( '_includes/excerpt-loop'); ?>
				
						<?php endwhile; ?>
						
					</section>	

					<?php else : ?>
				
						<p><?php _e( 'Sorry, nothing found.' ); ?></p>
				
					<?php endif; ?>		

					<?php get_sidebar(); ?>
				</div>
		
		<!-- /#content -->

<?php get_footer(); ?>