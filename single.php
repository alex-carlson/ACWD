<?php get_header(); ?>

<header class="singleHeader">
  <h2><a href="//acwd.me">Home</a></h2>
</header>

<?php while ( have_posts() ) : the_post(); ?>

	<div id="content">
		
		<?php get_template_part( '_includes/loop'); ?>

		<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

		<?php get_template_part( 'includes/post-nav'); ?>
			
	</div>

<?php endwhile; ?>
	
<?php get_footer(); ?>