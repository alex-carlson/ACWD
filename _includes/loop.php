<?php if(!is_single()) : global $more; $more=0 ; endif; //enable more link ?>

    <div <?php post_class( "post cf article $class"); ?>>
        <h1 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        <?php the_content(); ?>
        <?php the_date(); ?>
	</div>
<!-- /.post -->