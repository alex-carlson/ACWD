<?php if(!is_single()) : global $more; $more=0 ; endif; //enable more link ?>

    <div <?php post_class( "post trans cf article trans $class"); ?>>
        <h1 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
	</div>
<!-- /.post -->