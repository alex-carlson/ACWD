<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">


    <title>
        <?php if (is_home() || is_front_page()) { echo bloginfo( 'name'); } else { echo wp_title( ''); } ?>
    </title>

    <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>">
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/_public/main.css">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

    <?php wp_head(); ?> 
    <script src="<?php echo get_template_directory_uri(); ?>/_public/plugins.js"></script>

    <script src="//localhost:35729/livereload.js"></script>

    <script type="text/javascript">
        (function (doc) {
            var viewport = document.getElementById('viewport');
            if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i)) {
                doc.getElementById("viewport").setAttribute("content", "width=440px, minimum-scale=0.6, maximum-scale=.6, user-scalable = no");
                document.getElementById("hand-and-phone").className = "";
            } else if (navigator.userAgent.match(/iPad/i)) {
                doc.getElementById("viewport").setAttribute("content", "width=900px, minimum-scale=.8, maximum-scale=.8, user-scalable = no");
                document.getElementById("hand-and-phone").className = "";
            }
        }(document));
    </script>
    <script type="text/javascript">
        var templateUrl = '<?= get_bloginfo("template_url"); ?>';
    </script>
</head>

<body <?php body_class($class); ?>>
    <div class="cf">
        <header class="main cf col full trans">
            <svg version="1.1" id="logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 612 792" enable-background="new 0 0 612 792" xml:space="preserve">
                <polygon id="red" fill="#CF4343" points="232.5,592.2 156.8,461.2 232.5,330.1 383.8,330.1 459.5,461.2 383.8,592.2 "/>
                <polygon fill="#636466" points="232.5,330.1 157.6,199.8 232.5,68 383.9,68 459.6,199 383.9,330.1 "/>
                <polygon fill="#9C9EA0" points="459.1,460.7 383.8,330.2 459.6,199 611.4,461.2 "/>
                <polygon fill="#59595B" points="383.8,68 459.4,199.1 383.8,330.1 232.4,68 "/>
                <polygon fill="#6D6E70" points="232.5,330.1 156.8,461.2 6.6,461.2 157.6,199.8 "/>
                <polygon opacity="0.5" fill="#D0D1D3" enable-background="new    " points="459.6,199 383.8,330.1 232.5,330.1 384.3,68.3 "/>
                <polygon fill="#4E4E50" points="157.9,723.3 233.6,592.2 383.8,592.2 460.2,723.3 "/>
                <polygon fill="#59595B" points="156.8,461.2 233.6,592.2 157.9,723.3 6.6,461.2 "/>
                <polygon fill="#59595B" points="383.8,592.2 459.5,461.2 612,461.4 460.2,723.3 "/>
            </svg>


            <nav id="main-nav-wrap" class="hidden">
                <ul>
                    <li><a href="#About">About</a></li>
                    <li><a href="#Skills">Skills</a></li>
                    <li><a href="#Projects">Projects</a></li>
                    <li><a href="#Contact">Contact</a></li>
                </ul>
            </nav>
        </header>

        <div id="mainWrapper" class="jayZ col full cf">