<?php get_header(); ?>

	<div id="content" class="cf">
	
		<h1 class="page-title"><?php _e('404'); ?></h1>	
		<p><?php _e( 'Page not found.' ); ?></p>	
		
	</div>
		
<?php get_sidebar(); ?>
	
<?php get_footer(); ?>