<?php get_header(); ?>

	<div id="content" class="cf innerWrapper">
	
		<?php while ( have_posts() ) : the_post(); ?>
						
			<div class="content aboutContent">

				<script>
					var capabilities = ['Web Development', 'Game Development', 'CSS', 'HTML', 'SASS', 'Unity', 'Processing', 'Javascript', 'JQuery'];
					
					setInterval(function(){
						var rand = Math.floor((Math.random() * capabilities.length) + 1);
						$('#ticker').html(capabilities[rand]);
					}, 700);
				</script>
				
				<h1 class="page-title"><?php the_title(); ?></h1>

				<?php the_content(); ?>
				
				<?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
				
			</div>
								
		<?php endwhile; ?>

		<?php if(is_page( 'Capabilities' )) { ?>
			<span id="ticker">Web Development</span>
		<?php } ?>
		
	</div>
			
<?php get_footer(); ?>