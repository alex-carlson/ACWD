<aside id="sidebar" class="col right">

	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar') ) : ?>

		<section class="widget">
			<h4 class="widgettitle"><?php _e('Pages'); ?></h4>
			<ul>
			<?php wp_list_pages('title_li=' ); ?>
			</ul>
		</section>

		<section class="widget">
			<h4 class="widgettitle"><?php _e('Category'); ?></h4>
			<ul>
			<?php wp_list_categories('show_count=1&title_li='); ?>
			</ul>
		</section>

	<?php endif; ?>
	
	<!-- <section id="contact">
		<a href="https://www.facebook.com/acarlsongraphics" target="_blank">Facebook</a>
		<a href="https://twitter.com/alexcgraphics" target="_blank">Twitter</a>
		<a href="https://gitlab.com/u/alexcgraphics" target="_blank">Gitlab</a>
		<a href="mailto:alexcarlsongraphics@gmail.com" target="_blank">Email</a>
	</section> -->

</aside>
